// Manpreet Nagra, 09/07/2018, CSE 002-110
//// Class Cyclometer
// The purpose of this program is to compute distance and time elapsed for bike trips //
public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
   
    int secsTrip1=480; //time for the first trip
    int secsTrip2=3220; //time for the second trip
    int countsTrip1=1561; //number of rotations for trip one
    int countsTrip2=9037; //number of rotations for trip two
    
    double wheelDiameter=27.0, //diameter of the wheel
    PI=3.14159, //The value of pi
  feetPerMile=5280, //how many feet per mile
    inchesPerFoot=12, //how many inches per foot
    secondsPerMinute=60; //how many seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //measure of distance 
    
    System.out.println("Trip 1 took " +
          (secsTrip1/secondsPerMinute) +" minutes and had "+
                       countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
           (secsTrip2/secondsPerMinute) +" minutes and had "+
                        countsTrip2+" counts.");
    // Prints out the numbers that are stored in the variables 
    // Below gives Distance for each trip 1, 2
    // Gives total distance for bpth trips
    
distanceTrip1=countsTrip1*wheelDiameter*PI;
distanceTrip2=countsTrip2*wheelDiameter*PI;
    // Above gives distance in inches
    // (for each count, a rotation of the wheel travels
    // the diameter in inches times PI)
distanceTrip1/=inchesPerFoot*feetPerMile;
distanceTrip2/=inchesPerFoot*feetPerMile;
totalDistance=distanceTrip1+distanceTrip2;
    //Above gives total distance for both trips in inches 
//Print out the output data. 
System.out.println("Trip 1 was "+distanceTrip1+" miles");
System.out.println("Trip 2 was "+distanceTrip2+" miles");
System.out.println("The total distance was "+totalDistance+" miles");
    
    
  } //end of main method
}   // end of class