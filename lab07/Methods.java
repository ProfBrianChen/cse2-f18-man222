//Manpreet Nagra, 10/26/2018, CSE 002-110
//// Methods
// The purpose of this lab is to learn how to utilize methods

import java.util.Scanner;
import java.util.Random;
public class Methods {

    // the methods below define the string that the random interger generates and return them
    public static String adjectives(int adjective) {
        switch (adjective) {
            case 0: return "ugly";
            case 1: return "pretty";
            case 2: return "beautiful";
            case 3: return "scary";
            case 4: return "obnoxious";
            case 5: return "dull";
            case 6: return "bright";
            case 7: return "playful";
            case 8: return "tall";
            case 9: return "warm";
        }
        return null;
    }


    public static String nouns(int noun) {
        switch (noun) {
            case 0: return "cat";
            case 1: return "dog";
            case 2: return "fish";
            case 3: return "cow";
            case 4: return "baby";
            case 5: return "snake";
            case 6: return "spider";
            case 7: return "man";
            case 8: return "woman";
            case 9: return "boy";
        }
        return null;
    }

    public static String verb(int verb) {
        switch (verb) {
            case 0: return "played";
            case 1: return "slayed";
            case 2: return "tried";
            case 3: return "talked";
            case 4: return "walked";
            case 5: return "stared";
            case 6: return "yelled";
            case 7: return "cried";
            case 8: return "cooked";
            case 9: return "screamed";
        }
        return null;

    }

    public static String objectNoun(int objectNoun) {
        switch (objectNoun) {
            case 0: return "boardgame";
            case 1: return "piano";
            case 2: return "phone";
            case 3: return "baseball";
            case 4: return "basketball";
            case 5: return "volleyball";
            case 6: return "sofa";
            case 7: return "couch";
            case 8: return "swing";
            case 9: return "sticks";
        }
        return null;
    }
    // the main method gathers the returns from the methods above and creates sentences
    public static void main (String[]args) {
        Scanner scan = new Scanner(System.in);
        Random randomGenerator = new Random();
        int userEntered;
        System.out.println("Please enter the number of sentences you want: ");
        userEntered = scan.nextInt();
        if (userEntered== 0) {
        System.out.print("You don't want a sentence.");
        }
        else {
        int randomInput = randomGenerator.nextInt(10);
        System.out.print("The ");
        System.out.print(adjectives(randomInput));
        System.out.print(" ");
        System.out.print(nouns(randomInput));
        System.out.print(" ");
        System.out.print(verb(randomInput));
        System.out.print(" the ");
        System.out.print(objectNoun(randomInput));
        System.out.print(". ");
        int i = 1;
        while (i < userEntered) {
        randomInput = randomGenerator.nextInt(10);
        System.out.print("It ");
        System.out.print(verb(randomInput));
        System.out.print(" the ");
        System.out.print(objectNoun(randomInput));
        System.out.print(". ");
        i++;
        }
    }
}
}