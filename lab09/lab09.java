//Manpreet Nagra, 11/20/2018, CSE 002-110
//// Passing Arrays Inside Methods
// The purpose of this lab is to learn how to pass arrays between mathods.

import java.util.*;

public class lab09 {
    public static void main(String[] args ){
        int[] array0 = {1 ,2 ,3 ,4 ,5, 6, 7, 8 };
        int[] array1 = copy(array0);
        int[] array2 = copy(array0);
        inverter(array0);
        print(array0);
        inverterTwo(array1);
        print(array1);
        int[] array3 = inverterTwo(array2);
        print(array3);
    }
public static int[] copy(int[] myArray ){
   int[] myArrayTwo = new int[myArray.length];
   for (int i=0; i< myArray.length; i++){
    myArrayTwo[i] = myArray[i];
    }
    return  myArrayTwo;
}
public static void inverter(int[] myArray){
    for(int i = 0; i < myArray.length /2; i++){
    int reversed = myArray[i];
    myArray[i] = myArray[myArray.length - i - 1];
    myArray[myArray.length - i - 1] = reversed;
    }
}
public static int[] inverterTwo(int[] myArray){
    int[] theNewArray = copy(myArray);
    for(int i = 0; i < theNewArray.length /2; i++){
    int reversed = theNewArray[i];
    theNewArray[i] = theNewArray[theNewArray.length - i - 1];
    theNewArray[theNewArray.length - i - 1] = reversed;
    }
    return theNewArray;
}
public static void print(int[] myArray){
    for(int i = 0; i< myArray.length; i++){
        System.out.print(myArray[i] + " ");
    }
    System.out.println();
}
}