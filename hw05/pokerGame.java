// Manpreet Nagra, 09/28/2018, CSE 002-110
//// Poker Game
// The purpose of this program is exercise in using while loops

import java.util.Random;
import java.util.*;

public class pokerGame {
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    // Scanner will be used to ask the user how many hands to generate
    System.out.print("Enter the number of hands that you want to generate: ");
    int hands = myScanner.nextInt();
    Random generator = new Random();
    int cardOneOriginal=0;
    int cardTwoOriginal=0;
    int cardThreeOriginal=0;
    int cardFourOriginal=0;
    int cardFiveOriginal=0;
    int f=0;
    int t=0;
    int p=0;
    int o=0;
    String cardSuit = "";
	  String cardInt = "";
	  String BlankOfKind = "";
    
      for (int i=0; i < hands; i++){
        cardOneOriginal = generator.nextInt(52)+1;
        cardTwoOriginal = generator.nextInt(52)+1;
        cardThreeOriginal = generator.nextInt(52)+1;
        cardFourOriginal = generator.nextInt(52)+1;
        cardFiveOriginal = generator.nextInt(52)+1;
        double cardOne = cardOneOriginal % 13;
        double cardTwo = cardTwoOriginal % 13;
        double cardThree = cardThreeOriginal % 13;
        double cardFour = cardFourOriginal % 13;
        double cardFive = cardFiveOriginal % 13;
        //limits the card face values to 13
	if ((cardOne == cardTwo && cardOne==cardThree &&cardOne==cardFour) || 
      cardTwo == cardThree && cardTwo == cardFour && cardTwo == cardFive || 
      cardThree == cardFour && cardThree == cardFive && cardThree == cardOne || 
      cardFour == cardFive && cardFour == cardOne && cardFour == cardTwo ||
    	cardFive == cardOne && cardFive == cardTwo && cardFive == cardThree){
		BlankOfKind = "Four-of-a-kind";
		f++; // number of four-of-a-kind hands
    }
  if (cardOne == cardTwo && cardOne == cardFour|| 
      cardOne == cardTwo && cardOne == cardFive || 
      cardTwo == cardThree && cardTwo == cardFour ||
		  cardTwo == cardThree && cardTwo == cardFive || 
      cardThree == cardFour && cardThree == cardFive ||
      cardThree == cardFour && cardThree == cardOne||
      cardFour == cardFive && cardFour == cardOne||
      cardFour == cardFive && cardFour == cardTwo||
      cardFive == cardOne && cardFive == cardTwo||
      cardFive == cardOne && cardFive == cardThree){
		BlankOfKind = "Three-of-a-kind";
		t++; // number of three-of-a-kind hands
    }
    //boolean onePair = false
    //if (faceValue1 == faceValue2) 
  if (cardOne == cardTwo && cardTwo == cardThree || cardOne == cardTwo && cardTwo == cardFour || 
      cardOne == cardTwo && cardTwo == cardFive || 
      cardOne == cardThree && cardTwo == cardFour || cardOne == cardThree && cardTwo == cardFive || 
      cardOne == cardFour && cardThree == cardFive || 
      cardOne == cardFour && cardThree == cardTwo || 
    	cardOne == cardFive && cardTwo == cardThree || cardOne == cardFive && cardTwo == cardFour) {
		//if(onePair = true){
		p++;
		BlankOfKind = "Two-pair";
			// number of two-pair hands
	}
  if (cardOne == cardTwo || cardOne == cardThree || cardOne == cardFour || cardOne == cardFive || cardTwo == cardThree || 
      cardTwo == cardFour || cardTwo == cardFive || cardThree == cardFour || cardThree == cardFive || cardFour == cardFive ){
		BlankOfKind = "One-pair";
		o++;
    }
    //looks for one pair 
}
    double handD = hands;
    double probf= (f/handD);
    double probt = (t/handD);
    double probp = (p/handD);
    double probo = (o/handD); 
    
    System.out.printf("The probability of a Four-of-a-kind is: %4.3f\n",probf);
    System.out.printf("The probability of a Three-of-a-kind is: %4.3f\n",probt);
    System.out.printf("The probability of a Two-pair is: %4.3f\n" ,probp);
    System.out.printf("The probability of a One-pair is: %4.3f\n" ,probo);
    
    // prints out the probabilities
}
  
  }// end of main method