// Manpreet Nagra, 09/14/2018, CSE 002-110
//// Check
// The purpose of this program is to use the scanner class to obtain from the user the original cost 
// of the check, the percentage tip they wish to pay, and the number of ways the check will be split.

import java.util.*;
// The above imports the scanner class

public class Check{
    // main method required for every Java program
    public static void main(String[] args) {
      // In order to accept input, declare an instance of the scanner object
      Scanner myScanner = new Scanner( System.in );
      //Prompts the user for the original cost of the check 
      System.out.print("Enter the original cost of the check in the form xx.xx: ");
      //Accept the user input
      double checkCost = myScanner.nextDouble();
      //Prompts the user for the tip percentage that they wish to pay and accepts the input
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
      double tipPercent = myScanner.nextDouble();
      //Converts the percentage to a decimal value
      tipPercent /= 100;
      //Prompts the user for the number of people that went to dinner and accept the input
      System.out.print("Enter the number of people who went out to dinner: ");
      int numPeople = myScanner.nextInt();
      //Below prints out the output
double totalCost; //declares doubles
double costPerPerson; 
int dollars; //whole dollar amount of cost dimes, pennies;// for storing digits
             //for the cost $
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2
int dimes;
int pennies;
      
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
      

     
}
}