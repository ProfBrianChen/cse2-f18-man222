// Manpreet Nagra, 09/28/2018, CSE 002-110
//// Card Generator
// The purpose of this program is create a craps game in which the user can pick
// their own dice numbers or ask for two dice to be picked for them.
import java.util.Random;
import java.util.Scanner;

public class CrapsSwitch {
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    // scanner needed for user input
    System.out.print("Enter 1 if you want to randomly cast dice or enter 2 if you want to state the two dice: ");

    int choice = myScanner.nextInt();
    int valueOfDice1=0;
    int valueOfDice2=0;
    // declaring variables
    switch (choice) {
			case 1: Random generator = new Random();
      valueOfDice1 = generator.nextInt(6) +1;
      valueOfDice2 = generator.nextInt(6) +1;
      //asks for two dice values
        break;
			case 2: 
        while (valueOfDice1 < 1 || valueOfDice1 > 6){ 
			  System.out.print("Please enter a value of dice one: ");
			  valueOfDice1 = myScanner.nextInt();
		    }
		    while (valueOfDice2 < 1 || valueOfDice2 > 6){ 
			  System.out.print("Please enter a value for dice two: ");
			  valueOfDice2 = myScanner.nextInt();
		    }
        break;
		}
	// above is a loop in which if the user is not in the range, they will be asked for another dice value
    String output = "";
    int sum = valueOfDice1 + valueOfDice2; // the sum will decide the output
  switch (sum) {
			case 2: output = "Snake Eyes";
        break;
			case 3: output = "Ace Deuce";
				break;
			case 4: 
        switch (valueOfDice1){
          case 2: output = "Hard Four";
            break;
          default: output = "Easy Four";
            break;
         }
				break;
			case 5: output = "Fever Five";
				break;
			case 6:
        switch (valueOfDice1){
          case 3: output = "Hard Six";
            break;
          default: output = "Easy Six";
            break;
        }
				break;
      case 7:  output = "Seven Out";
      			break;
			case 8:
       switch (valueOfDice1){
          case 4: output = "Hard Eight";
            break;
          default: output = "Easy Eight";
            break;
       }
				break;
			case 9:  output = "Nine";
				break;
			case 10:
      switch (valueOfDice1){
          case 5: output = "Hard Ten";
            break;
          default: output = "Easy Ten";
            break;
      }
				break;
			case 11:  output = "Yo-leven";
				break;
      case 12:  output = "Boxcars";
				break;

  }
    
    System.out.println(output); // prints the output
  
  }
}// end of main method
