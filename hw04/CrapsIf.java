// Manpreet Nagra, 09/28/2018, CSE 002-110
//// Card Generator
// The purpose of this program is create a craps game in which the user can pick
// their own dice numbers or ask for two dice to be picked for them.
import java.util.Random;
import java.util.Scanner;

public class CrapsIf {
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    // scanner needed for user input
    System.out.print("Enter 1 if you want to randomly cast dice or enter 2 if you want to state the two dice: ");

    int choice = myScanner.nextInt();
    int valueOfDice1=0;
    int valueOfDice2=0;
    // declaring variables
    if (choice == 1) {
      Random generator = new Random();
      valueOfDice1 = generator.nextInt(6);
      valueOfDice2 = generator.nextInt(6);
      //asks for two dice values
    }
    if(choice == 2){
		while (valueOfDice1 < 1 || valueOfDice1 > 6){ 
			System.out.print("Please enter a value of dice one: ");
			valueOfDice1 = myScanner.nextInt();
		}
		while (valueOfDice2 < 1 || valueOfDice2 > 6){ 
			System.out.print("Please enter a value for dice two: ");
			valueOfDice2 = myScanner.nextInt();
		}
	} System.out.println(valueOfDice1 + " ," + valueOfDice2);
	// above is a loop in which if the user is not in the range, they will asked for another dice value
    String valueOfRolledDice = "";

      if(valueOfDice1 == 1){
        if(valueOfDice2 == 1){
          String SE = "Snake Eyes";
          System.out.println(SE +"!!");
        }
        if(valueOfDice2 == 2){
          String AD = "Ace Duece";
          System.out.println(AD +"!!");
        }
        if(valueOfDice2 == 3){
          String EF = "Easy Four";
          System.out.println(EF +"!!");
        }
        if(valueOfDice2 == 4){
          String FF = "Fever Five";
          System.out.println(FF +"!!");
        }
        if(valueOfDice2 == 5){
          String ES = "Easy Six";
          System.out.println(ES +"!!");
        }
        if(valueOfDice2 == 6){
          String SO = "Seven Out";
          System.out.println(SO +"!!");
        }
      }
      if(valueOfDice1 == 2){
        if(valueOfDice2 == 2){
          String HF = "Hard Four";
          System.out.println(HF +"!!");
        }
        if(valueOfDice2 == 3){
          String FF = "Fever Five";
          System.out.println(FF +"!!");
        }
        if(valueOfDice2 == 4){
          String ES = "Easy Six";
          System.out.println(ES +"!!");
        }
        if(valueOfDice2 == 5){
          String SO = "Seven Out";
          System.out.println(SO +"!!");
        }
        if(valueOfDice2 == 6){
          String EE = "Easy Eight";
          System.out.println(EE +"!!");
        }
      }
      if(valueOfDice1 == 3){
        if(valueOfDice2 == 3){
          String HF = "Hard Four";
          System.out.println(HF +"!!");
        }
        if(valueOfDice2 == 4){
          String HS = "Hard Six";
          System.out.println(HS +"!!");
        }
        if(valueOfDice2 == 5){
          String ES = "Easy Six";
          System.out.println(ES +"!!");
        }
        if(valueOfDice2 == 6){
          String SO = "Seven Out";
          System.out.println(SO +"!!");
        }
      }
      if (valueOfDice1 == 4){
        if(valueOfDice2 == 4){
          String HE = "Hard Eight";
          System.out.println(HE +"!!");
        }
        if(valueOfDice2 == 5){
          String N = "Nine";
          System.out.println(N +"!!");
        }
        if(valueOfDice2 == 6){
          String ET = "Easy Ten";
          System.out.println(ET +"!!");
        }
      }
      if (valueOfDice1 == 5){
		if(valueOfDice2 == 5){
		  String HT = "Hard Ten";
		  System.out.println(HT +"!!");
	    }
	    if(valueOfDice2 == 6){
	      String YL = "Yo-leven";
	      System.out.println(YL +"!!");
      	}
      }
      if (valueOfDice1 == 6){
	    if(valueOfDice2 == 6){
	      String B = "Boxcars";
	      System.out.println(B +"!!");
        }
      }
      if(valueOfDice2 == 1){
      if(valueOfDice1 == 2){
        String AD = "Ace Duece";
        System.out.println(AD +"!!");
      }
      if(valueOfDice1 == 3){
        String EF = "Easy Four";
        System.out.println(EF +"!!");
      }
      if(valueOfDice1 == 4){
        String FF = "Fever Five";
        System.out.println(FF +"!!");
      }
      if(valueOfDice1 == 5){
        String ES = "Easy Six";
        System.out.println(ES +"!!");
      }
      if(valueOfDice1 == 6){
        String SO = "Seven Out";
        System.out.println(SO +"!!");
      }
     }
    if(valueOfDice2 == 2){
      if(valueOfDice1 == 3){
        String FF = "Fever Five";
        System.out.println(FF +"!!");
      }
      if(valueOfDice1 == 4){
        String ES = "Easy Six";
        System.out.println(ES +"!!");
      }
      if(valueOfDice1 == 5){
        String SO = "Seven Out";
        System.out.println(SO +"!!");
      }
      if(valueOfDice1 == 6){
        String EE = "Easy Eight";
        System.out.println(EE +"!!");
     }
 	}
    if(valueOfDice2 == 3){
      if(valueOfDice1 == 4){
        String HS = "Hard Six";
        System.out.println(HS +"!!");
      }
      if(valueOfDice1 == 5){
        String ES = "Easy Six";
        System.out.println(ES +"!!");
      }
      if(valueOfDice1 == 6){
        String SO = "Seven Out";
        System.out.println(SO +"!!");
      }
    }
    if (valueOfDice2 == 4){ 
      if(valueOfDice1 == 5){
        String N = "Nine";
        System.out.println(N +"!!");
      }
      if(valueOfDice1 == 6){
        String ET = "Easy Ten";
        System.out.println(ET +"!!");
      }
    }
     if (valueOfDice2 == 5){
    if(valueOfDice1 == 6){
      String YL = "Yo-leven";
      System.out.println(YL +"!!");
    }
}
      // the list of slangs that will be printed if corresponding values are chosen.
}} //end of main method.