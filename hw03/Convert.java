//// Manpreet Nagra, 09/18/2018, CSE 002-110
//// Class Convert
// The purpose of this program is to practice writing code that enables the user 
// to input data and practice in performing arithmetic operations.

import java.util.*;
// The above imports the scanner class
public class Convert{
// main method required for every Java program
  public static void main(String args[]){
  // In order to accept input, declare an instance of the scanner object
    Scanner myScanner = new Scanner( System.in );
    //Prompts the user for the number of acres of land affected by hurricane precipitation
    System.out.println("Enter the number of acres of land affected by hurricane precipitation: "); //asks for user input
    double acresLand = myScanner.nextDouble(); //declares doubles(the acres of land) and gets the next user input
    
    System.out.print("Enter inches of rainfall in the affected area: "); //asks for user input
    double inchesRain = myScanner.nextDouble(); //declares doubles(inches of rain) and gets the next user input
    
    acresLand = acresLand * 0.0015625; //coverts to miles squared
    inchesRain = inchesRain * 0.0000157828; //converts to miles
    double cubicMilesRain = acresLand * inchesRain; //produces cubic miles of rain
    System.out.print("The cubic miles of rainfall in the affected area was: " + cubicMilesRain + "."); // Prints out the computation.
  
}
}