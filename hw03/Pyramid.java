//// Manpreet Nagra, 09/18/2018, CSE 002-110
//// Class Pyramid
// The purpose of this program is to prompt the user for the 
// dimensions of a pyramid and solve for the volume inside the pyramid

import java.util.*;
//The above imports the scanner class
public class Pyramid{
  //main method required for every Java program
  public static void main(String args[]){
    //In order to accept input, declare an instance of the scanner object

    Scanner myScanner = new Scanner( System.in);
    //prompts the user for the dimensions of the pyramid

    System.out.println("Enter the length of the pyramid: "); //asks the user for length
    double pyramidLength = myScanner.nextDouble(); //declares double and gets the user input

    System.out.println("Enter the height of the pyramid: ");//asks the user for height
    double pyramidHeight = myScanner.nextDouble(); //declares double and gets the user input

    double volumePyramid = (pyramidLength * pyramidLength * pyramidHeight) / 3;
    //declares double and computes the volume of the Pyramid.

    System.out.println("The volume of the pyramid is " + volumePyramid);
    //Prints out the computation.
        
  } //end of main method
} //end of class