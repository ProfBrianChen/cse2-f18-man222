//Manpreet Nagra, 10/12/2018, CSE 002-110
//// Display Pyramids
// The purpose of this lab is to learn nested loops and patterns
//that will help understand how to set up nested loops

import java.util.Scanner;

public class PatternD {
  //main method required in every java program
  public static void main(String[] args) {
	Scanner myScanner = new Scanner(System.in);
	//ask the user for the size of pyramid between 1 and 10
	System.out.print("Please enter the size(rows) of the pyramid between one through ten: ");
	int pyramidSize = 0;
	boolean correctSize = false;
	int numColumns;
	while(!correctSize){
  	//checks if the user entered an int and if they entered a number in the range 1-10
  	correctSize = myScanner.hasNextInt();
  	if(correctSize == true) {
    	pyramidSize = myScanner.nextInt();
    	if (pyramidSize < 1 || pyramidSize > 10) {
      	System.out.println("Please enter a number between 1 and 10: ");
      	correctSize = false;
    	}
  	}
  	else {
    	System.out.print("Not an integer, please enter another number: ");
    	myScanner.next(); //clears out the wrong input
  	}
	}
	for (int numRows = pyramidSize; numRows >= 1; --numRows) {
    	for (numColumns = numRows; numColumns >= 1; --numColumns){
        	System.out.print(" " + numColumns);
    	}
    	System.out.println();
	}
 }
}//end of main method
