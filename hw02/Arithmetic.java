//// Manpreet Nagra, 09/07/2018, CSE 002-110
//// Class Arithmetic
// The purpose of this program is to manipulate data stored in variables, 
//to runn simple calculations, and print the numerical output.



public class Arithmetic{
  
  public static void main(String args[]){
    ///Assumptions (inputting variables)
    
        //Number of pairs of pants
        int numPants = 3;
        //Cost per pair of pants
        double pantsPrice = 34.98;
    
        //Number of sweatshirts
        int numShirts = 2;
        //Cost per shirt
        double shirtsPrice = 24.99;
        
        //Number of belts
        int numBelts = 1;
        //cost per belt
        double beltCost = 33.99;
    
        //the tax rate
        double paSalesTax = 0.06;
    
    double totalCostPants; //total cost of pants
    double totalCostShirts; //total cost of shirts
    double totalCostBelts; //total cost of belts
    
    totalCostPants = numPants * pantsPrice; //total cost of pants
    totalCostShirts = numShirts * shirtsPrice; //total cost of shirts
    totalCostBelts = numBelts * beltCost; //total cost of belts
    
    System.out.println("The total cost of pants is $" + totalCostPants);
    System.out.println("The total cost of shirts is $" + totalCostShirts);
    System.out.println("The total cost of belts is $" + totalCostBelts);
// Above prints out the total cost of each item before tax.
    
    double salesTaxPants; //sales tax on pants
    double salesTaxShirts; //sales tax on shirts
    double salesTaxBelts; //sales tax on belts
    
    salesTaxPants =  totalCostPants * paSalesTax; //total tax on pants
        salesTaxPants = salesTaxPants * 100;
        int salesTaxPants1 = (int)salesTaxPants;
        salesTaxPants = (double)salesTaxPants1 / 100.0;
    //Limits the decimal places
    
    salesTaxShirts = totalCostShirts * paSalesTax; //total tax on shirts
        salesTaxShirts = salesTaxShirts * 100;
        int salesTaxShirts1 = (int)salesTaxShirts;
        salesTaxShirts = (double)salesTaxShirts1 / 100.0;
    //Limits the decimal places
    
    salesTaxBelts = totalCostBelts * paSalesTax; //total tax on belts
        salesTaxBelts = salesTaxBelts * 100;
        int salesTaxBelts1 = (int)salesTaxBelts;
        salesTaxBelts = (double)salesTaxBelts1 / 100.0;
    
// Above calculates sales tax charged buying all of each kind of item.
      
    System.out.println("The sales tax on pants is $" + salesTaxPants);
    System.out.println("The sales tax on shirts is $" + salesTaxShirts);
    System.out.println("The sales tax on belts is $" + salesTaxBelts);
// Above prints out the sales tax buying all of each kind of the item.     
    
    double totalCostPurchases; // total cost of purchases.
    
    totalCostPurchases = totalCostPants + totalCostShirts + totalCostBelts;
// Above gives the total cost of purchases before tax.
    
    System.out.println("The total cost of purchases before tax is $" + totalCostPurchases);
// Above prints out the total cost of purchases before tax
    
    double totalSalesTax; //total sales tax charged
    
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
// Above calculates the total sales tax.
    totalSalesTax = totalSalesTax * 100;
    int totalSalesTax1 = (int)totalSalesTax;
    totalSalesTax = (double)totalSalesTax1/100.0;
    //Limits the decimal places
    
    System.out.println("The total sales tax is $" + totalSalesTax);
// Above prints out total sales tax.
    
    double totalCostTransaction; //total cost of the transaction, including tax.
    
    totalCostTransaction = totalSalesTax + totalCostPurchases;
 // Above calculates total cost of the transaction, including tax.
    totalCostTransaction = totalCostTransaction * 100;
    int totalCostTransaction1 = (int)totalCostTransaction;
    totalCostTransaction = (double)totalCostTransaction1/100.0;
    System.out.println("The total cost of the transaction is $" + totalCostTransaction);
      //The Above gets rid of the extra decimals
    
  } //end of main method
} //end of class