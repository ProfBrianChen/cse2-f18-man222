//Manpreet Nagra, 11/27/2018, CSE 002-110
//// RemoveElements
// The purpose of this hw is to learn how to utilize arrays

//below allows the scanner class to be used
import java.util.Scanner;
public class RemoveElements{
    //main method required for every java program
    public static void main(String [] arg){
    Scanner scan=new Scanner(System.in); //Scanner used for user input
        int num[]=new int[10]; // an array of ten numbers 0-9
        int newArray1[];
        int newArray2[];
        int index,target;
    	String answer="";
        do{
          	System.out.print("Random input 10 ints [0-9]");
          	num = randomInput(num);
          	String out = "The original array is:";
          	out += listArray(num);
          	System.out.println(out);
         
          	System.out.print("Enter the index ");
          	index = scan.nextInt();
          	newArray1 = delete(num,index);
          	String out1="The output array is ";
          	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
          	System.out.println(out1);
         
            System.out.print("Enter the target value ");
          	target = scan.nextInt();
          	newArray2 = remove(num,target);
          	String out2="The output array is ";
          	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
          	System.out.println(out2);
              	 
          	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
          	answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
    }
    // the method below generates an array of 10 random integers between 0 to 9
    public static int[] randomInput(int[] num ){
            for(int i = 0; i < num.length; i++){
                num[i] = (int) (Math.random()*10);
            }
            return num;
    }
    // the method below creates a new array that has one member fewer than list
    public static int[] delete(int list[], int pos){
            int size = 0;
            if (list.length > 0) {
                size = list.length -1;
            }
            int newArray[] = new int[size];
            
            int elements = 0;
    
            for(int i =0; i < list.length; i++){
                if(i == pos){
                    continue;
                }
                newArray[elements] = list[i];
                elements++;
            }
            return newArray;
    }
    // the method below deletes all the elements that are equal to target,
        //returning a new list without all those new elements
    public static int[] remove(int list[], int target){
            int elements = 0;
            int index = -1;
            for(int i =0; i < list.length; i++){
                if(list[i] == target){
                    index = i;
                    continue;
                }
                elements++;
            }
            int newNewArray[] = new int[elements];
            elements = 0;
            for(int i = 0; i < list.length; i++) {
                if(list[i] == target){
                    continue;
                }
                newNewArray[elements] = list[i];
                elements++;
            }
            if (index == -1) {
                System.out.print("Your target value of " + target + " does not exist.");
            }
            System.out.println();
            return newNewArray;
            
    }
    public static String listArray(int num[]){
        	String out="{";
        	for(int j=0;j<num.length;j++){
          	if(j>0){
            	out+=", ";
          	}
          	out+=num[j];
        	}
        	out+="} ";
        	return out;
    }
}// this is the end of the remove elements class.
