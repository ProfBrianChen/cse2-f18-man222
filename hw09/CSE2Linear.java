//Manpreet Nagra, 11/27/2018, CSE 002-110
//// RemoveElements
// The purpose of this hw is to learn how to utilize arrays


import java.util.Scanner; // imports the scanner class
import java.util.Random; //imports the random class

public class CSE2Linear {
    //below is main method needed for all java programs
    public static void main(String args[]) {
        final int NUM_GRADES = 15;
        int[] userInput = new int[NUM_GRADES]; // uses NUM_GRADES as index
        
        int current = 0; // this is used for the current user Input
        
        Scanner scan = new Scanner(System.in); //scanner declared, will be used for user input
        System.out.println("Enter 15 ascending ints for final grades in CSE2: "); //asks the user for 15 ints
                // the for loop below checks if the user entered valid input by using if else statements
              for (int i = 0; i < userInput.length; i++) {
                  //System.out.print(i + "i " + userInput[i-1]); // this line was used to check if the array is being filled
                  System.out.print("Grade: ");
                      if(!scan.hasNextInt()){
                        scan.next();
                        System.out.print("Invalid input! Please enter an integer: ");
                        i--;
                      }
                      else{
                          current = scan.nextInt();
                          if (current < 0 || current > 100) {
                              System.out.println("Please enter a number between 0 and 100: ");
                              i--;
                          }
                          else{
                            if (i>0){
                                if (current < userInput[i-1]) {
                                    System.out.print("Please enter a greater number, ");
                                //System.out.print(i + "i " + userInput[i-1]);    
                                    i--;
                                }
                            else{
                                userInput[i] = current;
                            }
                            }
                          }
                        }
                }
            // the for loop below prints out the array
            for(int i = 0; i< userInput.length; i++){
            System.out.print(userInput[i] + " ");
            }
            System.out.println();
            
            // ask the user for a new input
            System.out.print("Enter a grade to search for: ");
            int intVal = scan.nextInt();
            int iterate = binarySearch(userInput, intVal);
            
            if (iterate == -1){
                System.out.print(intVal + " was not found with 4 iterations. ");    
            }
            else{
                System.out.print(intVal + " was found with " + iterate + " iterations. ");
            }
            //finding the user input in scrambled array
            System.out.println("Scambled array: ");
            randomScrambling(userInput);
            for (int i = 0; i < userInput.length; i++){
              System.out.print(userInput[i] + " ");
            }
             System.out.println();
         
            //use linear search to find the user input
            System.out.print("Enter a grade to search for: ");
            int intValue = scan.nextInt();
            int iterations = linearSearch(userInput, intValue);
            
            if (iterations == -1){
                System.out.print(intValue + " was not found with 15 iterations. ");    
            }
            else{
                System.out.print(intValue + " was found with " + iterations + " iterations. ");
            }
            
            
            
    } // end of main method
    // the method below uses linear search to scan the array and find user input
    public static int linearSearch(int[] userInput, int target){
    int counter = 0;
    int found = -1;
    for(int i = 0; i < userInput.length; i++) {
        if(userInput[i] == target){
            found = 1;
            counter++;
            break;
        }
        counter++;
    }
    if (found ==1)
    return counter;
    else 
    return found;
    }
    // the method below uses binary search to scan the array and find user input
    public static int binarySearch(int[] list, int key){
        int start = 0;
        int finalInt = list.length - 1;
        int counter =0;
        while (finalInt >= start) {
            int mid = (start + finalInt) / 2;
            if (key < list[mid]){
                finalInt = mid - 1;
            }
                else if (key == list[mid]){
                counter++;
                return mid;
            }
                else{
                start = mid + 1;
            }
        counter++;    
        }
         return -1; //not found
    }
    //this method scambles the array
    public static void randomScrambling(int[] num){
    {
    int index, temp;
    Random random = new Random();
    for (int i = num.length - 1; i > 0; i--){
        index = random.nextInt(i + 1);
        temp = num[index];
        num[index] = num[i];
        num[i] = temp;}
    }
    }

} // end of class

