//Manpreet Nagra, 10/12/2018, CSE 002-110
//// Encrypted X
// The purpose of this lab is to learn nested loops and patterns 
//that will help understand how to set up nested loops

import java.util.Scanner;

public class EncryptedX {
  //main method required in every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    //ask the user for the size of X between 0-100
    System.out.print("Please enter the size of the X between zero through one hundred: "); 
    int SquareSize = 0;
    boolean correctSize = false;
    int numColumns;
    while(!correctSize){
      //checks if the user entered an int and if they entered a number in the range 1-10
      correctSize = myScanner.hasNextInt();
      if(correctSize == true) {
        SquareSize = myScanner.nextInt();
        if (SquareSize <= -1 || SquareSize >= 101) {
          System.out.println("Please enter a number between 0 and 100: ");
          correctSize = false;
        }
      }
      else {
        System.out.print("Not an integer in the given range, please enter another number: ");
        myScanner.next(); //clears out the wrong input
      }
    }
    // Creates spaces at appropriate places in the grid so that it creates an X
    for (int numRows= 1; numRows <= SquareSize; numRows++) {
        for (numColumns= 1; numColumns <= SquareSize; numColumns++) { 
            if (numRows == numColumns || SquareSize - numColumns + 1 == numRows ) {
                System.out.print(" ");
            }
            else {
                System.out.print("*");
            }
        }
    System.out.println();
    }
  }
}