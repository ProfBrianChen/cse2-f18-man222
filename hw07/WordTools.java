//Manpreet Nagra, 11/04/2018, CSE 002-110
//// Methods
// The purpose of this hw is to learn how to utilize methods

//imports the Scanner 
import java.util.Scanner;
//imports the Pattern class used later
import java.util.regex.Pattern;
//imports the matcher class used later
import java.util.regex.Matcher;

public class WordTools {
    public static String sampleText(Scanner scan) {
        // this method asks the user to enter a String and stores the string
        System.out.print("Enter any text of your choice: ");
        String userText1 = scan.nextLine();
        return userText1;
    } // End of sampleText method
    
    public static char printMenu(String userText2) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Your text of choice was: ");
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println("Choose an option:");
        char userEntered = scan.next().charAt(0);
        //If the user enters an invalid response
        while(!(userEntered == 'c'  || userEntered == 'w' || userEntered == 'f' || userEntered == 'r' || userEntered == 's' || userEntered == 'q')){
            System.out.println("Not a valid input");
            System.out.println("c - Number of non-whitespace characters");
            System.out.println("w - Numberz of words");
            System.out.println("f - Find text");
            System.out.println("r - Replace all !'s");
            System.out.println("s - Shorten spaces");
            System.out.println("q - Quit");
            System.out.println("Choose an option:");
            scan.nextLine();    
            userEntered = scan.next().charAt(0);
        }
        return userEntered; //communicates the user input to main method
    } // End of the printMenu method
    
    public static int getNumOfNonWSCharacters(String nonWhiteSpace){
        int nWords =0;
        for(int w=0; w< nonWhiteSpace.length(); ++w){
            if(nonWhiteSpace.charAt(w) != ' '){
                nWords++;
            }
        }
        return nWords;
    }
    public static int getNumOfWords(String countOfWords){
        //below counts the words in the string by user getting rid of all spaces
        //Goes through the whole string and replaces all spaces
        int nWords =0;
        countOfWords = countOfWords.trim().replaceAll(" +"," ");
        for(int w =0; w < countOfWords.length(); ++w){
            if(countOfWords.charAt(w) == ' '){
                nWords++;
            }
        }
        nWords++;
        return nWords;
    } // End of getNumOfWords method
    public static int findText(String string1, String string2){
        //below finds how many times the text was entered
        int w =0;
        Pattern a=Pattern.compile(string1);
        Matcher b=a.matcher(string2);
        while(b.find()){
            w++;
        }
        return w;
    } //End of the findText method
    public static String replaceExclamation(String replacingEx){
        //this method replaces Exclamations with periods.
        String replaceExclamation = replacingEx.replace("!", ".");
        return replaceExclamation;
    } //End of method replaceExclamation 
    public static String shortenSpace(String shortenS){
        //this method replaces extra spaces with 1 space
        shortenS = shortenS.trim().replaceAll(" +"," ");
        return shortenS;
    }
    public static void main(String args[]) {
    Scanner scan = new Scanner(System.in);
    String userText1 = sampleText(scan);
    char c = printMenu(userText1);
    System.out.println(c);
    //below are if statements if that work according to user input
    if(c == 'c') {
        System.out.println("You have " + getNumOfNonWSCharacters(userText1) + " number of whitespaces.");
    }
    if(c == 'w') {
        System.out.println("You typed " + getNumOfWords(userText1) + " number of words.");
    }
    if(c == 'f') {
        System.out.println("Type a word to search in the string: "); 
        String textFind = scan.nextLine();
        System.out.println("The word: " + textFind + ", was entered " + findText(textFind, userText1) + " times.");
    }
    if(c == 'r') {
        System.out.println("Text with replaced exclamations: " + replaceExclamation(userText1));
    }
    if(c == 's') {
        System.out.println("Text with shortened spaces: " + shortenSpace(userText1));
  
    }
   if(c == 'q') {
        System.out.println("You asked to quit. Thank You"); 
    }
    }
   }