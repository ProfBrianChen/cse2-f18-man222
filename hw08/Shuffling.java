//Manpreet Nagra, 11/15/2018, CSE 002-110
//// Arrays
// The purpose of this hw is to learn how to utilize arrays

import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 

public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52];
for (int i=0; i<52; i++){
    cards[i]=rankNames[i%13]+suitNames[i/13]; // 52 cards with numbers and suits
}// end for loop
System.out.println();
int numCards = 0; 
int again = 1; 
int index = 51;
printArray(cards); // this method prints the whole deck
System.out.println();
System.out.println();
shuffle(cards);// this method shuffles the cards
System.out.println();
String[] hand = new String [numCards]; //this array will generate hand
while(again == 1){
    System.out.println("Please enter how many cards you would like in a hand");
    numCards = scan.nextInt(); // prompts the user for the number of hands
    while( numCards > 52){
        System.out.println("Invalid number! You entered a number out greater than the capacity of the deck. Enter a valid number: "); //number greater than 52
        scan.nextLine();
        numCards = scan.nextInt();
    }//end of inner while condition
    if(index == 0){ //this if statement will check if the user has taken out all the cards
        System.out.println("You have no cards left in the deck");
    }
    for(int i = 0; i < numCards; ++i){  //for loop for generating the hand
    hand = getHand(cards,index,numCards);  // implements the getHand method
    System.out.print(hand[i] + " ");
    }
    System.out.println();
    index = index - numCards;
    System.out.println("Enter a 1 if you want another hand drawn");
    again = scan.nextInt();
} //end of while loop
}// end of main method

public static void printArray(String[] cards){
for (int i=0; i<52; i++){ 
  System.out.print(cards[i]+" "); 
}
System.out.println();
}// end of printArray method

public static void shuffle(String[] list){
    Random rn = new Random(); //generates random int
    for (int i = 0; i < list.length; i++){
        int shuffled = rn.nextInt(52);
        System.out.print(list[shuffled]+ " ");
    } //end of loop
}//end of shuffle method

public static String[] getHand(String[] list, int index, int numCards){
    Random rn = new Random(); //generates random int
    String[] hand = new String[index + 1]; // changes the index start point from 0 to 1
    for(int i = 0; i < numCards; i++){
        int newRandom = rn.nextInt(index + 1);
        hand[i] = list[newRandom]; // saves the array
    } //end of loop
    return hand; //returns the hand to the main method
}// end of gethand method
} // end of class