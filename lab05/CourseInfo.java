// Manpreet Nagra, 10/10/2018, CSE 002-110
//// 
// The purpose of this program is to practice using while loops to check
// if the user inputs correct information and if they don't, ask for 
// another input

//import the scanner class
import java.util.*;
public class CourseInfo {
// main method required for every Java program
public static void main(String[] args) {
  //scanner needed for user input
  Scanner myScanner = new Scanner(System.in);
  
  //print statement that asks the user for their course number
  System.out.print("Please enter the course number: ");
  int courseNum = 0;
  boolean correctInt = false;
  while(!correctInt){
    // checks if there's an int
    correctInt = myScanner.hasNextInt();
    if(correctInt == true){
      courseNum = myScanner.nextInt();
      if (courseNum < 0){
        System.out.println("Please enter another valid course number: ");
        correctInt = false;
      }
    }
    else { 
      System.out.print("Not an integer, please enter another number: "); 
      myScanner.next();// clears it out
    }
  }
    
  System.out.print("Please enter the department name: ");
  String depName = "";
  boolean correctString = false;
  while(!correctString){
    // checks if there's an int
    correctString = myScanner.hasNext();
    if(correctString == true){
      depName = myScanner.next();
    }
    
    else { 
      System.out.print("Not a correct input, please enter department name: "); 
      myScanner.next();// clears it out
    }
  }
   
  System.out.print("Please enter the instructor name: ");
  String insName = "";
  correctString = false;
  while(!correctString){
    // checks if there's an String
    correctString = myScanner.hasNext();
    if(correctString == true){
      insName = myScanner.next();
      }
    
    else {
      System.out.print("Not the correct input, please enter the instructor name: "); 
      myScanner.next(); // clears it out
    }
  }
  System.out.print("Please enter the number of times course meets: ");
  int NumOfMeets = 0;
  correctInt = false;
  while(!correctInt){
    // checks if there's an int
    correctInt = myScanner.hasNextInt();
    if(correctInt == true){
      NumOfMeets = myScanner.nextInt();
      if (NumOfMeets < 0){
        System.out.println("Please enter another valid number: ");
        correctInt = false;
      }
     }  
        else { 
      System.out.println("Not an integer, please enter another number: "); 
      myScanner.next();// clears it out
    }
  }
  System.out.print("Please enter the time the class meets: ");
  int timeOfClass = 0;
  correctInt = false;
  while(!correctInt){
    // checks if there's an int
    correctInt = myScanner.hasNextInt();
    if(correctInt == true){
      timeOfClass = myScanner.nextInt();
      if (timeOfClass < 0){
        System.out.println("Please enter another valid time: ");
        correctInt = false;
      }
        }  
        else { 
      System.out.print("Not an integer, please enter another number: "); 
      myScanner.next();// clears it out
    }
  }    
  System.out.print("Please enter the number of students in the class: ");
  int StudentsInClass = 0;
  correctInt = false;
  while(!correctInt){
    // checks if there's an int
    correctInt = myScanner.hasNextInt();
    if(correctInt == true){
      StudentsInClass = myScanner.nextInt();
      if (StudentsInClass < 0){
        System.out.println("Please enter another valid number: ");
        correctInt = false;
      }
        }  
        else { 
      System.out.print("Not an integer, please enter another number: "); 
      myScanner.next();// clears it out
    }
  }
}
  }//end of main method
